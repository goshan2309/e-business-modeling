#include "my_math.h"

double Sqr(double x) {
    return x * x;
}

double GetExpectedValue(const std::vector<double>& values) {

    double expected_value = 0;

    for (int i = 0; i < values.size(); ++i) {
        expected_value += values[i];
    }

    expected_value /= values.size();

    return expected_value;
}

double GetDeviationValue(const std::vector<double>& values) {

    double expected_val = GetExpectedValue(values);

    double disp_val = 0;
    for (int i = 0; i < values.size(); ++i) {
        disp_val += Sqr(expected_val - values[i]);
    }
    disp_val /= (values.size() - 1);

    double deviation_val = sqrt(disp_val);

    return deviation_val;
}

