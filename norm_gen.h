#pragma once

#include <iostream>
#include <fstream>
#include <iomanip>

#include <vector>

#include <random>
#include <cmath>

#include "my_math.h"

class NormalDistributionGenerator {
public:
    NormalDistributionGenerator(double expected, double deviation);

    double Get();

    void Test();

    void GraphicTest();

    void FormalTest();

private:
    double InverseFunction(double arg1, double arg2) const;

    double ProbabilityDensityFunction(double x) const;

    double CumulativeDistributionFunction(double x) const;

    const double expected_;
    const double deviation_;

    std::mt19937_64 local_generator1_;
    std::mt19937_64 local_generator2_;

    std::uniform_real_distribution<double> distribution_;
};
