#pragma once

#include <iostream>

#include <vector>
#include <string>

#include "norm_gen.h"

enum class BlockState {
	Sleep,
	Work,
};

std::ostream& operator<<(std::ostream& os, const BlockState& state);

struct Block {

	Block(std::string name, int work_time_expected, int work_time_deviation);

	void Link(Block& destination);

	void Refresh();

	int GetWorkTime();

	const std::string name_;

	int inputs_count_;
	int active_inputs_counter_;

	std::vector<Block*> outputs_;

	BlockState state_;

	int timer_;

	NormalDistributionGenerator work_time_generator_;
	
};
