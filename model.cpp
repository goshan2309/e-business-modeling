#include "model.h"

using namespace std;


vector<Block*> GetBlocks() {

	// Create

	auto podvoz = new Block("Podvoz", 10, 3);
	auto sortirovka_komplekt = new Block("Sort komp", 10, 3);

	auto zapaivanie = new Block("Zapai", 10, 3);
	auto ustanovka_po = new Block("Ust PO", 10, 3);

	auto sborka_korpusa = new Block("Sbor korp", 10, 3);
	auto obrabotka_korpusa = new Block("Obr korp", 10, 3);

	auto sborka_korpusa_i_platy = new Block("Sb k & p.", 10, 3);

	auto obrabotka_figurok = new Block("Obr fig", 10, 3);

	auto test_doski_i_figurok = new Block("Test d & f", 10, 3);

	auto konechnaya_upakovka = new Block("Kon upak", 10, 3);

	auto vivoz_na_sklad = new Block("Vivoz", 10, 3);

	// Bind

	podvoz->Link(*sortirovka_komplekt);

	sortirovka_komplekt->Link(*zapaivanie);
	sortirovka_komplekt->Link(*sborka_korpusa);
	sortirovka_komplekt->Link(*obrabotka_figurok);

	zapaivanie->Link(*ustanovka_po);
	sborka_korpusa->Link(*obrabotka_korpusa);

	ustanovka_po->Link(*sborka_korpusa_i_platy);
	obrabotka_korpusa->Link(*sborka_korpusa_i_platy);

	sborka_korpusa_i_platy->Link(*test_doski_i_figurok);
	obrabotka_figurok->Link(*test_doski_i_figurok);

	test_doski_i_figurok->Link(*konechnaya_upakovka);
	konechnaya_upakovka->Link(*vivoz_na_sklad);

	// necessary for circle
	vivoz_na_sklad->Link(*podvoz);

	// Return

	vector<Block*> result = {
		podvoz,
		sortirovka_komplekt,

		zapaivanie,
		ustanovka_po,

		sborka_korpusa,
		obrabotka_korpusa,

		sborka_korpusa_i_platy,

		obrabotka_figurok,

		test_doski_i_figurok,

		konechnaya_upakovka,

		vivoz_na_sklad,
	};

	return result;
}

Model::Model(vector<Block*>&& blocks, int max_time, int boards_per_cycle, double defect_prob) :
	blocks_(move(blocks)),
	counter_for_block_(blocks_.size()),
	state_by_second_and_block_id_(
		max_time + 1,
		vector<BlockState>(blocks_.size())
	),

	max_time_(max_time),
	boards_per_cycle_(boards_per_cycle),

	defect_prob_(defect_prob)

{}

void Model::Simulate() {

	blocks_[0]->state_ = BlockState::Work;
	blocks_[0]->timer_ = blocks_[0]->GetWorkTime();


	for (int t = 0; t <= max_time_; ++t) {

		GetStates(t);

		UpdateStates();

		UpdateTimers();
	}

	PrintToFile();
}

void Model::GetStates(int t) {
	for (int i = 0; i < blocks_.size(); ++i) {
		state_by_second_and_block_id_[t][i] = blocks_[i]->state_;
	}
}

void Model::UpdateStates() {

	for (int i = 0; i < blocks_.size(); ++i) {

		if (blocks_[i]->state_ == BlockState::Work && blocks_[i]->timer_ == 0) {
			blocks_[i]->state_ = BlockState::Sleep;
			for (int j = 0; j < blocks_[i]->outputs_.size(); ++j) {
				++(blocks_[i]->outputs_[j]->active_inputs_counter_);
			}
			++counter_for_block_[i];
		}

		else if (blocks_[i]->state_ == BlockState::Sleep && blocks_[i]->active_inputs_counter_ == blocks_[i]->inputs_count_) {

			blocks_[i]->state_ = BlockState::Work;
			blocks_[i]->timer_ = blocks_[i]->GetWorkTime();
			blocks_[i]->active_inputs_counter_ = 0;
		}
	}

	// necessary
	// MISSED WAKEUP
	if (blocks_[0]->state_ == BlockState::Sleep && blocks_[0]->active_inputs_counter_ == blocks_[0]->inputs_count_) {

		blocks_[0]->state_ = BlockState::Work;
		blocks_[0]->timer_ = blocks_[0]->GetWorkTime();
		blocks_[0]->active_inputs_counter_ = 0;
	}
}

void Model::UpdateTimers() {
	for (int i = 0; i < blocks_.size(); ++i) {
		if (blocks_[i]->state_ == BlockState::Work) {
			--(blocks_[i]->timer_);
		}
	}
}

void Model::PrintToFile() {

	ofstream fout("output.txt");

	const int WIDTH = 10;

	fout << setw(WIDTH) << " ";
	for (int i = 0; i < blocks_.size(); ++i) {
		fout << setw(WIDTH) << blocks_[i]->name_;
	}
	fout << "\n";

	for (int t = 1; t <= max_time_; ++t) {
		fout << setw(WIDTH) << t;
		for (int i = 0; i < blocks_.size(); ++i) {
			fout << setw(WIDTH) << state_by_second_and_block_id_[t][i];
		}
		fout << "\n";
	}

	int full_cycles = *min_element(counter_for_block_.begin(), counter_for_block_.end());

	int total_product = full_cycles * boards_per_cycle_;

	int bad_product = total_product * defect_prob_;

	int good_product = total_product - bad_product;

	fout << setprecision(3) << fixed

		<< "Total time (mins):" << endl
		<< max_time_ << endl

		<< "Boards per cycle: " << endl
		<< boards_per_cycle_ << endl

		<< "Defect prob:" << endl
		<< defect_prob_ << endl << endl

		<< "Full cycles:" << endl
		<< full_cycles << endl

		<< "Total product:" << endl
		<< total_product << endl

		<< "Defected boards:" << endl
		<< bad_product << endl

		<< "Normal product:" << endl
		<< good_product << endl;


	cout << setprecision(3) << fixed

		<< "Total time (mins):" << endl
		<< max_time_ << endl

		<< "Boards per cycle: " << endl
		<< boards_per_cycle_ << endl

		<< "Defect prob:" << endl
		<< defect_prob_ << endl << endl

		<< "Full cycles:" << endl
		<< full_cycles << endl

		<< "Total product:" << endl
		<< total_product << endl

		<< "Defected boards:" << endl
		<< bad_product << endl

		<< "Normal product:" << endl
		<< good_product << endl;



}
