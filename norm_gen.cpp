#include "norm_gen.h"

using namespace std;
using ll = int64_t;


NormalDistributionGenerator::NormalDistributionGenerator(double expected, double deviation) :

    local_generator1_(random_device()()),
    local_generator2_(random_device()()),

    distribution_(0.0, 1.0),

    expected_(expected),
    deviation_(deviation)
{}


double NormalDistributionGenerator::Get() {

    return
        InverseFunction(
            distribution_(local_generator1_),
            distribution_(local_generator2_)
        );
}

void NormalDistributionGenerator::Test() {
    GraphicTest();
    FormalTest();
}

void NormalDistributionGenerator::GraphicTest() {

    //const double step = 0.2;
    const double step = deviation_ / 5;
    const int n = 1'000'000;

    //int number_of_counters = 2 * (ceil(3 * deviation_ / step));
    int number_of_counters = 30;

    double x_min = expected_ - (number_of_counters / 2) * step;
    double x_max = expected_ + (number_of_counters / 2) * step;

    vector<ll> counters(number_of_counters + 1);

    double sum = 0;
    vector<double> values(n);

    for (double& value : values) {

        value = Get();
        if (x_min < value && value < x_max) {
            sum += value;
            ++counters[floor((value - x_min) / step)];
        }
    }

    double experimental_expected = sum / n;

    sum = 0;
    for (const double& value : values) {
        sum += Sqr(value - experimental_expected);
    }
    double experimental_dispersion = sum / (n - 1);


    // FOR USER

    const ll WIDTH = 15;

    cout << setprecision(6) << fixed << endl << endl <<
        "NORMAL DISTRIBUTION GENERATOR GRAPHICAL TEST" << endl << endl <<
        "Experimental expected: " << experimental_expected << endl <<
        "Theoretical expected: " << expected_ << endl <<
        "Experimental standard deviation: " << sqrt(experimental_dispersion) << endl <<
        "Theoretical standard deviation: " << deviation_ << endl << endl;


    cout << "Prob density func values:\n" <<
        setw(WIDTH) << "x" <<
        setw(WIDTH) << "experiment" <<
        setw(WIDTH) << "theory" << endl <<
        setprecision(3) << fixed;

    for (ll i = 0; i < number_of_counters; ++i) {

        double mid = x_min + i * step + step / 2;

        cout <<
            setw(WIDTH) << mid <<
            setw(WIDTH) << static_cast<double>(counters[i]) / n / step <<
            setw(WIDTH) << ProbabilityDensityFunction(mid) << endl;
    }


    // FOR PYTHON
    ofstream os("norm_dist_output.txt");
    os << setprecision(3) << fixed;

    os << "x = [";
    for (int i = 0; i < number_of_counters; ++i) {
        double mid = x_min + i * step + step / 2;
        os << mid << ", ";
    }
    os << "]" << endl;

    os << "experiment = [";
    for (int i = 0; i < number_of_counters; ++i) {
        double mid = x_min + i * step + step / 2;
        os << static_cast<double>(counters[i]) / n / step << ", ";
    }
    os << "]" << endl;

    os << "theory = [";
    for (int i = 0; i < number_of_counters; ++i) {
        double mid = x_min + i * step + step / 2;
        os << ProbabilityDensityFunction(mid) << ", ";
    }
    os << "]" << endl;

};

void NormalDistributionGenerator::FormalTest() {

    //const double step = 0.2;
    const double step = deviation_ / 5;
    const int n = 1'000'000;

    //int number_of_counters = 2 * (ceil(3 * deviation_ / step));
    int number_of_counters = 30;

    double x_min = expected_ - (number_of_counters / 2) * step;
    double x_max = expected_ + (number_of_counters / 2) * step;

    vector<ll> counters(number_of_counters + 1);

    for (int i = 0; i < n; ++i) {

        double value = Get();
        if (x_min < value && value < x_max) {
            ++counters[floor((value - x_min) / step)];
        }
    }



    double sum = 0;

    for (ll i = 0; i < number_of_counters; ++i) {

        double left = i * step + x_min;
        double right = left + step;

        double exp_prob = static_cast<double>(counters[i]) / n;
        double theory_prob =
            CumulativeDistributionFunction(right) -
            CumulativeDistributionFunction(left);

        // cout << theory_prob << " " << exp_prob << endl;

        sum += Sqr(exp_prob - theory_prob) / theory_prob;
    }

    double result = sum * n;
    cout << setprecision(6) << fixed << endl << endl <<
        "NORMAL DISTRIBUTION GENERATOR FORMAL TEST:" << endl << endl <<
        "Pearson criterion: " << endl <<
        "Chi: " << result << endl <<
        "Beta: " << number_of_counters - 3 << endl;
}


double NormalDistributionGenerator::InverseFunction(double arg1, double arg2) const {

    return expected_ + (deviation_ * cos(2 * PI * arg1) * sqrt(-2 * log(arg2)));
}

double NormalDistributionGenerator::ProbabilityDensityFunction(double x) const {

    double factor1 = 1.0 / (deviation_ * sqrt(2 * PI));
    double factor2 = exp(-Sqr(x - expected_) / (2 * Sqr(deviation_)));

    return factor1 * factor2;
}

double NormalDistributionGenerator::CumulativeDistributionFunction(double x) const {
    return 0.5 * (1.0 + erf((x - expected_) / sqrt(2 * Sqr(deviation_))));
}
